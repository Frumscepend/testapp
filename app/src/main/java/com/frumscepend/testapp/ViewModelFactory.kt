package com.frumscepend.testapp

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.main.viewmodel.AddViewModel
import com.frumscepend.testapp.main.viewmodel.BackViewModel
import com.frumscepend.testapp.main.viewmodel.StoreItemViewModel
import com.frumscepend.testapp.main.viewmodel.StoreViewModel

/**
 * A creator is used to inject the product ID into the ViewModel
 */
class ViewModelFactory private constructor(
        private val application: Application,
        private val productRepository: ProductDataSource
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(StoreViewModel::class.java) ->
                        StoreViewModel(application, productRepository)
                    isAssignableFrom(BackViewModel::class.java) ->
                        BackViewModel(application, productRepository)
                    isAssignableFrom(StoreItemViewModel::class.java) ->
                        StoreItemViewModel(application, productRepository)
                    isAssignableFrom(AddViewModel::class.java) ->
                        AddViewModel(application, productRepository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(application,
                            Injection.provideTasksRepository(application.applicationContext))
                            .also { INSTANCE = it }
                }


        @VisibleForTesting fun destroyInstance() {
            INSTANCE = null
        }
    }
}
