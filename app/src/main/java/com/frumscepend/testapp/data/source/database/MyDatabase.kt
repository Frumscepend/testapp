package com.frumscepend.testapp.data.source.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.frumscepend.testapp.data.Product

/**
 * The Room MyDatabase that contains the Task table.
 */
@Database(entities = [(Product::class)], version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun taskDao(): ProductsDao

    companion object {

        private var INSTANCE: MyDatabase? = null

        private val lock = Any()

        fun getInstance(context: Context): MyDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            MyDatabase::class.java, "Products.db")
                            .build()
                }
                return INSTANCE!!
            }
        }
    }

}