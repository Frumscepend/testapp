package com.frumscepend.testapp.data.source.database

import android.support.annotation.VisibleForTesting
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.utils.AppExecutors

/**
 * DB Data source providing data from local sqlite database
 */

class ProductDatabaseDataSource private constructor(
        private val appExecutors: AppExecutors,
        private val productsDao: ProductsDao
) :ProductDataSource{

    override fun getProducts(callback: ProductDataSource.LoadProductsCallback) {
        appExecutors.networkIO.execute {
            val products = productsDao.getAllProducts()
            appExecutors.mainThread.execute {
                if (products.isEmpty()) {
                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable()
                } else {
                    callback.onProductsLoaded(products)
                }
            }
        }
    }

    override fun getProduct(id: String, callback: ProductDataSource.ProductCallback) {
        appExecutors.networkIO.execute {
            val product: Product? = productsDao.getProductById(id)
            appExecutors.mainThread.execute {
                if (product != null) {
                    callback.onSuccess(product)
                } else {
                    callback.onFail()
                }
            }
        }
    }

    override fun getAvailableProducts(callback: ProductDataSource.LoadProductsCallback) {
        appExecutors.networkIO.execute {
            val products = productsDao.getAllActiveProducts()
            appExecutors.mainThread.execute {
                if (products.isEmpty()) {
                    // This will be called if there is no available products.
                    callback.onDataNotAvailable()
                } else {
                    callback.onProductsLoaded(products)
                }
            }
        }
    }

    override fun saveProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        appExecutors.networkIO.execute {
            //waiting for long operation
            Thread.sleep(5000)
            productsDao.updateProduct(product.count, product.cost, product.name, product.id)
            appExecutors.mainThread.execute {
                callback.onReady()
            }
        }
    }

    override fun createProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        appExecutors.networkIO.execute {
            //waiting for long operation
            Thread.sleep(5000)
            productsDao.insertProducts(product)
            appExecutors.mainThread.execute {
                callback.onReady()
            }
        }
    }

    override fun deleteProduct(productId: String) {
        appExecutors.networkIO.execute {
            productsDao.deleteProduct(productId)
            appExecutors.mainThread.execute {

            }
        }
    }

    override fun saveAllProducts(products: List<Product>, callback: ProductDataSource.InsertProductsCallback) {
        appExecutors.networkIO.execute {
            productsDao.insertAllProducts(products)
            appExecutors.mainThread.execute {
                callback.onReady()
            }
        }
    }

    override fun buyProduct(product: Product, callback: ProductDataSource.ProductCallback) {
        appExecutors.networkIO.execute {
            //waiting for long operation
            Thread.sleep(3000)
            val existingProduct = productsDao.getProductById(product.id)
            if (existingProduct.count - 1 >= 0) {
                val result = productsDao.updateProduct(existingProduct.count - 1, existingProduct.cost, existingProduct.name, existingProduct.id)
                val productRes = productsDao.getProductById(product.id)
                appExecutors.mainThread.execute {
                    if (result > 0) {
                        callback.onSuccess(productRes)
                    } else {
                        callback.onFail()
                    }
                }
            } else{
                appExecutors.mainThread.execute {
                    callback.onFail()
                }
            }
        }
    }

    companion object {
        private var INSTANCE: ProductDatabaseDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, productsDao: ProductsDao): ProductDatabaseDataSource {
            if (INSTANCE == null) {
                synchronized(ProductDatabaseDataSource::javaClass) {
                    INSTANCE = ProductDatabaseDataSource(appExecutors, productsDao)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

}