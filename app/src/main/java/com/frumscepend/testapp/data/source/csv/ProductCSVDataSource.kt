package com.frumscepend.testapp.data.source.csv

import android.content.Context
import android.support.annotation.VisibleForTesting
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.utils.AppExecutors
import com.opencsv.CSVReader
import java.io.InputStreamReader

/**
 * CSV Data source providing data from local csv file
 */

class ProductCSVDataSource private constructor(
        private val appExecutors: AppExecutors,
        private val context: Context
) : ProductDataSource {

    override fun getProducts(callback: ProductDataSource.LoadProductsCallback) {
        appExecutors.diskIO.execute {
            try
            {
                val inputStreamReader = InputStreamReader(context.assets.open("data.csv"))
                val reader = CSVReader(inputStreamReader)
                var line = reader.readNext()
                val products = ArrayList<Product>()
                while (line != null) {
                    val product = Product()
                    product.name = line[0]
                    product.cost = line[1].toDouble()
                    product.count = line[2].toInt()
                    products.add(product)
                    line = reader.readNext()
                }
                appExecutors.mainThread.execute {
                    callback.onProductsLoaded(products)
                }
            }
            catch (e:Exception) {
                callback.onDataNotAvailable()
            }
        }
    }
    override fun getProduct(id: String, callback: ProductDataSource.ProductCallback) {
        throw NotImplementedError()
    }

    override fun getAvailableProducts(callback: ProductDataSource.LoadProductsCallback) {
        throw NotImplementedError()
    }

    override fun saveProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        throw NotImplementedError()
    }

    override fun createProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        throw NotImplementedError()
    }

    override fun deleteProduct(productId: String) {
        throw NotImplementedError()
    }

    override fun saveAllProducts(products: List<Product>, callback: ProductDataSource.InsertProductsCallback) {
        throw NotImplementedError()
    }

    override fun buyProduct(product: Product, callback: ProductDataSource.ProductCallback) {
        throw NotImplementedError()
    }

    companion object {
        private var INSTANCE: ProductCSVDataSource? = null

        @JvmStatic
        fun getInstance(appExecutors: AppExecutors, context: Context): ProductCSVDataSource {
            if (INSTANCE == null) {
                synchronized(ProductCSVDataSource::javaClass) {
                    INSTANCE = ProductCSVDataSource(appExecutors, context)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}