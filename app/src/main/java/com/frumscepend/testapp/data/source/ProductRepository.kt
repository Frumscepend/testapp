package com.frumscepend.testapp.data.source

import com.frumscepend.testapp.data.Product

class ProductRepository (
        private val productDatabaseDataSource: ProductDataSource,
        private val productCSVDataSource: ProductDataSource
): ProductDataSource {

    private fun initLoadData(callback: ProductDataSource.LoadProductsCallback){
        productCSVDataSource.getProducts(object : ProductDataSource.LoadProductsCallback {
            override fun onProductsLoaded(products: List<Product>) {
                productDatabaseDataSource.saveAllProducts(products, object : ProductDataSource.InsertProductsCallback {
                    override fun onReady() {
                        callback.onProductsLoaded(products)
                    }
                })
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    private fun initAvailableLoadData(callback: ProductDataSource.LoadProductsCallback){
        productCSVDataSource.getProducts(object : ProductDataSource.LoadProductsCallback {
            override fun onProductsLoaded(products: List<Product>) {
                productDatabaseDataSource.saveAllProducts(products, object : ProductDataSource.InsertProductsCallback {
                    override fun onReady() {
                        getAvailableProducts(callback)
                    }
                })
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    override fun getProducts(callback: ProductDataSource.LoadProductsCallback) {
        productDatabaseDataSource.getProducts(object : ProductDataSource.LoadProductsCallback {
            override fun onProductsLoaded(products: List<Product>) {
                callback.onProductsLoaded(products)
            }

            override fun onDataNotAvailable() {
                /**
                 * If local DB is empty then read info from CSV and write into local DB
                 */
                initLoadData(callback)
            }
        })
    }

    override fun getProduct(id: String, callback: ProductDataSource.ProductCallback) {
        productDatabaseDataSource.getProduct(id, callback)
    }

    override fun getAvailableProducts(callback: ProductDataSource.LoadProductsCallback) {
        productDatabaseDataSource.getAvailableProducts(object : ProductDataSource.LoadProductsCallback {
            override fun onProductsLoaded(products: List<Product>) {
                callback.onProductsLoaded(products)
            }

            override fun onDataNotAvailable() {
                /**
                 * If local DB is empty then read info from CSV and write into local DB
                 */
                initAvailableLoadData(callback)
            }
        })

    }

    override fun saveProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        productDatabaseDataSource.saveProduct(product, callback)
    }

    override fun saveAllProducts(products: List<Product>, callback: ProductDataSource.InsertProductsCallback) {
        productDatabaseDataSource.saveAllProducts(products, callback)
    }

    override fun createProduct(product: Product, callback: ProductDataSource.InsertProductsCallback) {
        productDatabaseDataSource.createProduct(product, callback)
    }

    override fun deleteProduct(productId: String) {
        productDatabaseDataSource.deleteProduct(productId)
    }

    override fun buyProduct(product: Product, callback: ProductDataSource.ProductCallback) {
        productDatabaseDataSource.buyProduct(product, callback)
    }

    companion object {

        private var INSTANCE: ProductRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param productDatabaseDataSource the backend data source
         * *
         * @param productCSVDataSource  the device storage data source
         * *
         * @return the [ProductRepository] instance
         */
        @JvmStatic fun getInstance(
                productDatabaseDataSource: ProductDataSource,
                productCSVDataSource: ProductDataSource) =
                INSTANCE ?: synchronized(ProductRepository::class.java) {
                    INSTANCE ?: ProductRepository(productDatabaseDataSource, productCSVDataSource)
                            .also { INSTANCE = it }
                }


        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}