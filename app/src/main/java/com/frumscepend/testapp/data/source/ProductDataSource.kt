package com.frumscepend.testapp.data.source

import com.frumscepend.testapp.data.Product

interface ProductDataSource {

    interface LoadProductsCallback {

        fun onProductsLoaded(products: List<Product>)

        fun onDataNotAvailable()
    }

    interface InsertProductsCallback {

        fun onReady()
    }

    interface ProductCallback {

        fun onSuccess(product: Product)

        fun onFail()
    }

    fun getProducts(callback: LoadProductsCallback)

    fun getProduct(id: String, callback: ProductCallback)

    fun getAvailableProducts(callback: LoadProductsCallback)

    fun saveProduct(product: Product, callback: InsertProductsCallback)

    fun saveAllProducts(products: List<Product>, callback: InsertProductsCallback)

    fun createProduct(product: Product, callback: InsertProductsCallback)

    fun deleteProduct(productId: String)

    fun buyProduct(product: Product, callback: ProductCallback)
}