package com.frumscepend.testapp.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable
import java.util.UUID

@Entity(tableName = "products")
data class Product constructor(
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "cost") var cost: Double = 0.0,
        @ColumnInfo(name = "count") var count: Int = 0,
        @PrimaryKey @ColumnInfo(name = "id") var id: String = UUID.randomUUID().toString()
): Serializable {
    val isEmpty
        get() = count == 0
}