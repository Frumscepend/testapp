package com.frumscepend.testapp.main.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.frumscepend.testapp.R
import com.frumscepend.testapp.databinding.ActivityMainBinding
import com.frumscepend.testapp.main.databindingadapter.BottomNavigationViewDataBindingAdapter
import com.frumscepend.testapp.main.fragment.BackFragment
import com.frumscepend.testapp.main.fragment.StoreFragment
import com.frumscepend.testapp.utils.replaceFragmentInActivity

class MainActivity : AppCompatActivity() {

    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        showStoreFragment()
        activityMainBinding.listener = object : BottomNavigationViewDataBindingAdapter.NavigationListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId)
                {
                    R.id.navigation_front -> {
                        showStoreFragment()
                        return true
                    }
                    R.id.navigation_back -> {
                        showBackFragment()
                        return true
                    }
                }
                return false
            }
        }
    }

    fun showStoreFragment() =
        StoreFragment.newInstance().also {
            replaceFragmentInActivity(it, R.id.contentFrame)
        }

    fun showBackFragment() =
        BackFragment.newInstance().also {
            replaceFragmentInActivity(it, R.id.contentFrame)
        }
}
