package com.frumscepend.testapp.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.support.annotation.StringRes
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.utils.SingleLiveEvent


class AddViewModel (
        private val context: Application,
        private val productRepository: ProductDataSource
): AndroidViewModel(context) {
    val product: ObservableField<Product> = ObservableField()
    var callback: ProductDataSource.InsertProductsCallback? = null
    fun onSaveClick(name: String, cost: String, count: String) {
        product.get()?.let {
            // If editing old product
            it.name = name
            it.count = count.toIntOrNull()?.let { it } ?: kotlin.run { 0 }
            it.cost = cost.toDoubleOrNull()?.let { it } ?: kotlin.run { 0.0 }
            productRepository.saveProduct(it, callback!!)
        } ?: run {
            // If creating new product
            val productNew = Product()
            productNew.name = name
            productNew.count = count.toIntOrNull()?.let { it } ?: kotlin.run { 0 }
            productNew.cost = cost.toDoubleOrNull()?.let { it } ?: kotlin.run { 0.0 }
            productRepository.createProduct(productNew, callback!!)

        }
    }

    fun start(newProduct: Product) {
        product.set(newProduct)
    }
}