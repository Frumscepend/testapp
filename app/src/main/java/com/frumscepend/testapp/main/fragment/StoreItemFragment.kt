package com.frumscepend.testapp.main.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.frumscepend.testapp.R
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.databinding.FragmentStoreItemBinding
import com.frumscepend.testapp.events.DataUpdateEvent
import com.frumscepend.testapp.main.activity.MainActivity
import com.frumscepend.testapp.main.viewmodel.StoreItemViewModel
import com.frumscepend.testapp.utils.obtainViewModel
import com.frumscepend.testapp.utils.setupSnackbar
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class StoreItemFragment : Fragment(){

    private lateinit var fragmentStoreItemBinding: FragmentStoreItemBinding
    var product: Product = Product()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentStoreItemBinding.viewmodel?.let {
            /*
            Creating snack bar
             */
            view?.setupSnackbar(this, it.message, Snackbar.LENGTH_LONG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentStoreItemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_item, container, false)
        fragmentStoreItemBinding.viewmodel = (activity as MainActivity).obtainViewModel(product.id, StoreItemViewModel::class.java)
        fragmentStoreItemBinding.viewmodel?.let { model ->
            model.start(product)
            model.onBuyEvent = object : StoreItemViewModel.OnBuyEvent{
                override fun onBuy() {
                    /**
                     * Notify everyone about data update
                     */
                    EventBus.getDefault().post(DataUpdateEvent())
                }

            }
        }
        return fragmentStoreItemBinding.root
    }

    companion object {
        fun newInstance() = StoreItemFragment()
    }
}