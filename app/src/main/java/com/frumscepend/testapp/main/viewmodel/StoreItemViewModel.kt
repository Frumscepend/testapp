package com.frumscepend.testapp.main.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.support.annotation.StringRes
import com.frumscepend.testapp.R
import com.frumscepend.testapp.data.Product
import com.frumscepend.testapp.data.source.ProductDataSource
import com.frumscepend.testapp.utils.SingleLiveEvent

class StoreItemViewModel (
        private val context: Application,
        private val productRepository: ProductDataSource
): AndroidViewModel(context) {
    val product: ObservableField<Product> = ObservableField()
    val message = SingleLiveEvent<Int>()
    var onBuyEvent: OnBuyEvent? = null
    fun onBuyClick() {
        productRepository.buyProduct(product = product.get()!!, callback = object : ProductDataSource.ProductCallback {
            override fun onSuccess(product: Product) {
                //updating product and invoke callback
                this@StoreItemViewModel.product.set(product)
                onBuyEvent?.onBuy()
            }

            override fun onFail() {
                showMessage(R.string.invalid_buy)
            }

        })
    }

    fun start(newProduct: Product) {
        product.set(newProduct)
    }

    private fun showMessage(@StringRes message: Int) {
        this.message.value = message
    }

    interface OnBuyEvent{
        fun onBuy()
    }
}