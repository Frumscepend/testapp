package com.frumscepend.testapp.main.databindingadapter

import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.support.v4.view.ViewPager
import android.databinding.adapters.ListenerUtil
import android.databinding.InverseBindingListener
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import com.frumscepend.testapp.R


@BindingMethods(
        BindingMethod(type = RecyclerView::class, attribute = "app:adapter", method = "setAdapter")
)
class RecyclerViewDataBindingAdapter private constructor() {

    init {
        throw UnsupportedOperationException()
    }
}

/**
 * Recycler view extension
 */

@BindingAdapter("dividerDirection")
fun RecyclerView.setItemDecoration(oldDirection: Int, newDirection: Int) {
    if (oldDirection != newDirection) {
        val decoration = DividerItemDecoration(context, newDirection)
        decoration.setDrawable(context.getDrawable(R.drawable.list_divider))
        val old = ListenerUtil.trackListener(this, decoration, R.id.recyclerViewBack)
        if (old != null) {
            removeItemDecoration(old)
        }
        addItemDecoration(decoration)
    }
}