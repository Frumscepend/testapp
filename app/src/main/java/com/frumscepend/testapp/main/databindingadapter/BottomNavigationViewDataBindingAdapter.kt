package com.frumscepend.testapp.main.databindingadapter

import android.support.design.widget.BottomNavigationView
import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.view.MenuItem


@BindingMethods(BindingMethod(type = BottomNavigationView::class, attribute = "app:onNavigationItemSelected", method = "setOnNavigationItemSelectedListener"))
class BottomNavigationViewDataBindingAdapter {
    interface NavigationListener {
        fun onNavigationItemSelected(item: MenuItem): Boolean
    }
}