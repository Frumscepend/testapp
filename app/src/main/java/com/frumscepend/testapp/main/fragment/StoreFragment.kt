package com.frumscepend.testapp.main.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.frumscepend.testapp.databinding.FragmentStoreBinding
import com.frumscepend.testapp.events.DataUpdateEvent
import com.frumscepend.testapp.main.activity.MainActivity
import com.frumscepend.testapp.main.adapter.StoreAdapter
import com.frumscepend.testapp.main.viewmodel.StoreViewModel
import com.frumscepend.testapp.utils.obtainViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class StoreFragment: Fragment(){

    private lateinit var fragmentStoreBinding: FragmentStoreBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentStoreBinding = FragmentStoreBinding.inflate(inflater, container, false).apply {
            viewmodel = (activity as MainActivity).obtainViewModel(StoreViewModel::class.java)
        }
        fragmentStoreBinding.viewmodel?.let { model ->
            model.storeAdapter = StoreAdapter(viewModel = model, fm = childFragmentManager)
        }
        return fragmentStoreBinding.root
    }

    /**
     * Updating data by request
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: DataUpdateEvent) {
        fragmentStoreBinding.viewmodel?.start()
    }

    override fun onResume() {
        super.onResume()
        fragmentStoreBinding.viewmodel?.start()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    companion object {
        fun newInstance() = StoreFragment()
    }
}