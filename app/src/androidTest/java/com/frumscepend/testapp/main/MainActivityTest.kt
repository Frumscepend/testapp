package com.frumscepend.testapp.main

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.toPackage
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.widget.TextView
import com.frumscepend.testapp.R
import com.frumscepend.testapp.main.activity.AddProductActivity
import com.frumscepend.testapp.main.activity.MainActivity
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class) @LargeTest
class MainActivityTest {
    @get:Rule var activityTestRule = IntentsTestRule(MainActivity::class.java)

    @Test fun changeFragmentTest(){
        onView(withId(R.id.navigation_back)).perform(click())
        onView(withId(R.id.recyclerViewBack)).check(matches(isDisplayed()))

        onView(withId(R.id.navigation_front)).perform(click())
        onView(withId(R.id.viewPagerStore)).check(matches(isDisplayed()))
    }

    fun getText(matcher: Matcher<View>): String? {
        val stringHolder = arrayOf<String?>(null)
        onView(matcher).perform(object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isAssignableFrom(TextView::class.java)
            }

            override fun getDescription(): String {
                return "getting text from a TextView"
            }

            override fun perform(uiController: UiController, view: View) {
                val tv = view as TextView //Save, because of check in getConstraints()
                stringHolder[0] = tv.text.toString()
            }
        })
        return stringHolder[0]
    }

    @Test fun buyTest(){
        Thread.sleep(2000)
        val matcher = allOf(withId(R.id.textViewAmountValue), isDisplayed())
        val startedText = getText(matcher)
        onView(allOf(withId(R.id.buttonBuy), isDisplayed())).perform(click())
        Thread.sleep(5000)
        onView(matcher).check(matches(not(withText(startedText))))
    }

    @Test fun addActivityTest(){
        onView(withId(R.id.navigation_back)).perform(click())
        onView(withId(R.id.buttonAdd)).perform(click())
        intended(allOf(toPackage("com.frumscepend.testapp"),
                hasComponent(AddProductActivity::class.java.name)))
    }
}